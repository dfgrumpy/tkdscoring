



$(document).bind("mobileinit", function(){
    $.mobile.defaultPageTransition = 'slide';
    $.mobile.ajaxEnabled = false;
});



$(document).bind('pageshow', function() {
    // load data for page when page is shown
    notifyFired = false;
    loader.overlayDisplay();
    loader.loadDataForPage();

    try {
        notify.init();

        // clean up event date field
        $("#eventdate").parent().addClass("ui-shadow-inset");
        $("#eventdate").parent().css('border', '');

    }
    catch (e){   }





});

$(document).bind('pageinit', function() {
    // click handlers
    $('.eventSaveBtn').on('click', function(){
        var evName = $('#eventname').val();
        var evDate = $('#eventdate').val();
        var disciplines = [];
        
        $("input:checkbox[name=discipline]:checked").each(function()  {
            disciplines.push($(this).val());
        });
        
        if (evName != '' && evDate != '' && utils.isDate(evDate) && disciplines.toString() != ''){
            postSaveLocation = $(this).attr('href');
            scoresApp.webdb.saveNewEvent(evName, evDate, disciplines);
        } else {
            notify.alert('Please select an event Name, Date, and Discipline');
        }
    });


    // click handlers
    $('.eventUpdateBtn').on('click', function(){
        var evName = $('#eventname').val();
        var evDate = $('#eventdate').val();
        var disciplinesNew = [];
        var disciplinesCurr = [];

        $("input:checkbox[name=discipline]:checked").each(function()  {
            disciplinesNew.push($(this).val());
        });

        $("input:checkbox[name=selectedDiscipline]:checked").each(function()  {
            disciplinesCurr.push($(this).val());
        });

        if (evName != '' && evDate != '' && utils.isDate(evDate) && (disciplinesCurr.toString() != '' || disciplinesNew.toString() != '')){
            thisEvent = utils.getUrlVars()['eventid'];
            postSaveLocation = $(this).attr('href');
            scoresApp.webdb.saveUpdateEvent(thisEvent, evName, evDate, disciplinesNew, disciplinesCurr);
        } else {
            notify.alert('Please select an event Name, Date, and Discipline');
        }
    });


    $('.deleteEventBtn').on('click', function(){

        var n = notify.confirm('<p>Delete this event?</p>', 'eventDelete')

    });





    $('.selectedDisciplineEdit').live("change", function(event, ui) {


        if ($(this).attr('checked') == undefined && $(this).attr('status') != 0){
            if (! notifyFired){
                notify.alert('Removing this discipline will also remove the saved scores.');
            }
        }
    });

    $('#competitorNameDialogBtn').on('click', function(e){

        if ( $('#competitorName').val() != '' && $('#competitorName').val() != tempCompetitorName){
            tempCompetitorName = $('#competitorName').val()
            $('a[competitor="'+tempCompetitorID+'"]').find('span').eq(1).html(tempCompetitorName );
            scoresApp.webdb.updateCompetitorName(tempCompetitorID, tempCompetitorName);
        }
        $('#popupContestantName').popup( "close" );

        var compSelect = $("#competitorNameSelect");
        compSelect[0].selectedIndex = 0;
        compSelect.selectmenu("refresh");
    });

    $('.completeScoresBtn').on('click', function(e){
        var $checkResult = utils.validateWinnerChoices();

        if ($checkResult == 1){
            notify.alert('Please make sure only one of each place is chosen.');
            return false;
        } else if ($checkResult == 2){
            notify.alert('Please select a first, second, and third place winner as necessary.');
            return false;
        }

        var $places = utils.buildWinnerArray();
        scoresApp.webdb.finalizeDisciplineResults($places, utils.getUrlVars().disciplineid, utils.getUrlVars().eventid);

    });

    $('#competitorNameSelect').on('change', function(e){

        if ($(this).val() != 0){
            $('#competitorName').val($(this).val());
        }
    });


});


// loader functions

var loader = function() {

    var OverlayDisplay = function(){

        var currHref = $(location).attr('href').split('/');
        var thisPage = currHref[currHref.length - 1].split(".")[0];
        if (thisPage != 'index' && thisPage != 'about' && thisPage != ''){
            utils.showOverlay();
        }
    };

    var LoadDataForPage = function() {

        var urlVarArray = utils.getUrlVars();
        var currHref = $(location).attr('href').split('/');
        var thisPage = currHref[currHref.length - 1].split(".")[0];





        if (thisPage == 'newevent') {

            scoresApp.webdb.loadDisciplines(thisPage);
        } else if (thisPage == 'eventlistip') {
            scoresApp.webdb.loadEvents(thisPage, 0);
        } else if (thisPage == 'event'){
            if (utils.getUrlVars().source == 0 || utils.getUrlVars().source == undefined){
                $('#listHeaderBtn').attr('href', './eventlistip.html');
            } else {
                $('#listHeaderBtn').attr('href', './eventlistcomp.html');
            }

           if (validator.checkNumber(urlVarArray['eventid']) == 0){
               utils.relocate('./index.html');
               return;
            }
            scoresApp.webdb.loadEventDetail(urlVarArray['eventid'], 'event');
        } else if (thisPage == 'editevent'){

            if (validator.checkNumber(urlVarArray['eventid']) == 0){
                utils.relocate('./index.html');
                return;
            }
            scoresApp.webdb.loadEventEdit(urlVarArray['eventid'], 'editevent');
        } else if (thisPage == 'scoring'){

            if (validator.checkNumber(urlVarArray['disciplineid']) == 0){
                utils.relocate('./index.html');
                return;
            }
            scoresApp.webdb.loadScoringHeaderDetail(urlVarArray['disciplineid'], 'scoring');
        } else if (thisPage == 'results'){

            if (validator.checkNumber(urlVarArray['disciplineid']) == 0){
                utils.relocate('./index.html');
                return;
            }
            scoresApp.webdb.loadScoringHeaderDetail(urlVarArray['disciplineid'], 'results');
        } else if (thisPage == 'eventlistcomp'){
            scoresApp.webdb.loadEvents(thisPage, 2);
        }
    };

    var loadEventData = function(page, eventid) {
        tourneyApp.webdb.loadPageEvent(page, eventid);
    };

    return {
        loadDataForPage : function() {
            LoadDataForPage();
        },
        overlayDisplay: function(){
            OverlayDisplay();
        }

    }

}();



var mycallbacks = function(){
    
    var NewEventSaveComplete = function(id){
        var loc = postSaveLocation + '?eventid=' + id;
        utils.relocate(loc, 1);
    };

    var PopulatePage = function(page, data){

        var baseTemplate = '';
        var thisItem = '';
        var thisHTML = '';

        if (page == 'newevent'){    
            // add checkboxes to new event page
            baseTemplate = templates.disciplineLi();
            thisItem = '';
            thisHTML = '';
            for (var i = 0; i < data.rows.length; i++){
                thisItem = data.rows.item(i);
                thisHTML = baseTemplate;
                thisHTML = thisHTML.replace('~name', thisItem.name);
                thisHTML = thisHTML.replace(/~id/g, thisItem.ID);
                $('#disciplineChecks').append(thisHTML);
            }
           $('#disciplineChecksDiv').trigger('create');



            utils.hideOverlay();
        } else if (page == 'eventlistip'){
            if ( ! data.rows.length){
                $('#noItemsWarningDiv').show();
                utils.hideOverlay();
                return;
            }

            $('#noItemsWarningDiv').hide();
            $thisEventLUL = $('#eventlistUL');
            $thisEventLUL.empty();
            baseTemplate = templates.eventListIp();
            thisItem = '';
            thisHTML = '';
            for (var i = 0; i < data.rows.length; i++){
                thisItem = data.rows.item(i);
                thisHTML = baseTemplate;
                thisHTML = thisHTML.replace('~eventid', thisItem.ID);
                thisHTML = thisHTML.replace('~eventname', thisItem.eventname);
                thisHTML = thisHTML.replace('~eventdate', utils.dateFormat(thisItem.eventdate));
                thisHTML = thisHTML.replace('~source', '0');
                $thisEventLUL.append(thisHTML).trigger('create');
            }
            $thisEventLUL.listview('refresh');
            utils.hideOverlay();

        } else if (page == 'event') {

            var $thisData = data.rows.item(0);

            if (! data.rows.length){
                utils.relocate('./index.html?showMessageA=Unable to load event');
                return;
            }
            $('#eventNameTxt').html($thisData.eventname);
            $('#eventDateTxt').html(utils.dateFormat($thisData.eventdate));

            utils.changeHeadertheme($thisData.status, 1);

            if ($thisData.status == 2){
                $('#eventEditLink').hide();
            } else {
                //noinspection JSJQueryEfficiency
                $('#eventEditLink').attr('href', $('#eventEditLink').attr('href') + $thisData.ID);
            }

            $('.deleteEventBtn').attr('eventid', $thisData.ID);

            scoresApp.webdb.loadDisciplinesForEvent($thisData.ID);

        } else if (page == 'editevent') {

            var $thisEvent = data.event.rows.item(0);
            var $thisSelDisciplines = data.selDisciplines;
            var $thisDisciplines = data.disciplines;

            var selItems = PopulateDisciplineEditList($thisSelDisciplines);
            PopulateDisciplineBaseList($thisDisciplines, selItems);

            $('#eventname').val($thisEvent.eventname);
            $('#eventdate').val(utils.dateFormatForm($thisEvent.eventdate));


            utils.hideOverlay();

        } else if (page == 'scoring') {


            var $thisData = data.rows.item(0);

            if (! data.rows.length){
                utils.relocate('./index.html?showMessageA=Unable to load event discipline');
                return;
            }
            $('#eventNameTxt').html($thisData.eventname);
            $('#eventDisciplineTxt').html($thisData.name);

            if  ($thisData.status == 2){
                $('#finalizeDisciplineBtn').hide();
            }


            utils.changeHeadertheme($thisData.status, 2);

            $bntHref = $('#eventMainBtn').attr('href') + $thisData.eventid;
            $finalHref = './results.html?eventid=' + $thisData.eventid + '&disciplineid=' + $thisData.ID;

            $('#finalizeDisciplineBtn').attr('href', $finalHref);
            $('#eventMainBtn').attr('href', $bntHref);
            scoresApp.webdb.loadDisciplinesForEventScoring( $thisData.eventid, $thisData.ID);
            scoresApp.webdb.getCompetitorsForDiscipline($thisData.ID, $thisData.status);

        } else if (page == 'results') {
            var $thisData = data.rows.item(0);

            $eventBackHref = './scoring.html?eventid=' + $thisData.eventid + '&disciplineid=' + $thisData.ID;
            $('#eventBackBtn').attr('href', $eventBackHref);

            $('#eventNameTxt').html($thisData.eventname);
            $('#eventDisciplineTxt').html($thisData.name);
            scoresApp.webdb.getPossibleWinners($thisData.ID);

        } else if (page == 'eventlistcomp') {
            if (! data.rows.length){
                $('#noItemsWarningDiv').show();
                utils.hideOverlay();
                return;
            }

            $('#noItemsWarningDiv').hide();
            $thisEventLUL = $('#eventlistUL');
            $thisEventLUL.empty();
            baseTemplate = templates.eventListComp();
            thisItem = '';
            thisHTML = '';
            for (var i = 0; i < data.rows.length; i++){
                thisItem = data.rows.item(i);
                thisHTML = baseTemplate;
                thisHTML = thisHTML.replace('~eventid', thisItem.ID);
                thisHTML = thisHTML.replace('~eventname', thisItem.eventname);
                thisHTML = thisHTML.replace('~eventdate', utils.dateFormat(thisItem.eventdate));
                thisHTML = thisHTML.replace('~source', '1');
                $thisEventLUL.append(thisHTML).trigger('create');
            }
            $thisEventLUL.listview('refresh');
            utils.hideOverlay();
        }

    };

    var PopulateDisciplineList = function(data){
        var thisItem = '';
        var eDiscTemplate = templates.eventDisciplineLi();
        var $thisData = data.rows;
        var $thisEventDLUL = $('#eventDisciplinelistUL');
        if (! data.rows.length){
           $('#noDisciplineWarning').show();
        } else {

           for (var i = 0; i < data.rows.length; i++){
               thisItem = data.rows.item(i);
               thisHTML = eDiscTemplate;
               thisHTML = thisHTML.replace('~discipline', thisItem.name);
               thisHTML = thisHTML.replace('~theme', utils.statusToTheme(thisItem.status));
               thisHTML = thisHTML.replace(/~eid/g, thisItem.eventid);
               thisHTML = thisHTML.replace(/~did/g, thisItem.ID);
               $thisEventDLUL.append(thisHTML).trigger('create');
           }

            $thisEventDLUL.listview('refresh');
        }

        utils.hideOverlay();
    };

    var PopulateDisciplineEditList = function(data){
        var thisItem = '';
        var items = [];
        var eDiscTemplate = templates.eventDisciplineEditLi();
        var $thisData = data.rows;
        var $thisEventDLUL = $('#disciplinesList');
        if (! data.rows.length){
            $('#noDisciplineWarning').show();
        } else {

            for (var i = 0; i < data.rows.length; i++){
                thisItem = data.rows.item(i);

                items.push(thisItem.discipline);
                thisHTML = eDiscTemplate;
                thisHTML = thisHTML.replace('~discipline', thisItem.name);
                thisHTML = thisHTML.replace('~theme', utils.statusToTheme(thisItem.status));
                thisHTML = thisHTML.replace(/~id/g, thisItem.ID);
                thisHTML = thisHTML.replace('~status', thisItem.status);
                $thisEventDLUL.append(thisHTML);
            }
            $('#disciplinesListDiv').trigger('create');
        }
        return items;
    };

    var PopulateDisciplineListScoring = function(data){

        if (! data.rows.length){
            $('#switchDisciplineListDiv').hide();
            return;
        }
        var $thisEventDLUL = $('#switchDisciplineList');
        var eDiscTemplate = templates.eventDisciplineLi();
        var $thisData = data.rows;

        for (var i = 0; i < data.rows.length; i++){
            thisItem = data.rows.item(i);
            thisHTML = eDiscTemplate;
            thisHTML = thisHTML.replace('~discipline', thisItem.name);
            thisHTML = thisHTML.replace('~theme', utils.statusToTheme(thisItem.status));
            thisHTML = thisHTML.replace(/~eid/g, thisItem.eventid);
            thisHTML = thisHTML.replace(/~did/g, thisItem.ID);
            $thisEventDLUL.append(thisHTML).trigger('create');
        }
        $thisEventDLUL.listview('refresh');


    };

    var PopulateDisciplineBaseList = function(data, selected){
        var thisItem = '';
        var eDiscTemplate = templates.disciplineLi();
        var $thisData = data.rows;
        var $thisEventDLUL = $('#disciplineChecks');

            for (var i = 0; i < data.rows.length; i++){
                thisItem = data.rows.item(i);

                if ($.inArray(thisItem.ID, selected) == -1){

                    thisHTML = eDiscTemplate;
                    thisHTML = thisHTML.replace('~name', thisItem.name);
                    thisHTML = thisHTML.replace(/~id/g, thisItem.ID);

                    $thisEventDLUL.append(thisHTML);
                }

            }
            $('#disciplineAvailDiv').trigger('create');

    };


    var PopulateScoringFields = function(data, status){
        var $thisData = data.rows;
        if (status == 2){
            var $thisTemplate = templates.scoreEntryLIRO();
            $('#legendDisplay').hide();
            $('#scoreTitle').html('Scoring Results...');
        } else {
            var $thisTemplate = templates.scoreEntryLI();
            $('#legendDisplay').show();
            $('#scoreTitle').html('Enter scores...');
        }
        var $thisUL = $('#competitorList');
        for (var i = 0; i < data.rows.length; i++){
            thisItem = data.rows.item(i);
            thisHTML = $thisTemplate;

            if (status == 2 && thisItem.scorea == 0 && thisItem.scoreb == 0 && thisItem.scorec == 0){
                continue;
            }

            if (status == 2){
                thisHTML = thisHTML.replace('~placeTxt', utils.placeToText(thisItem.place));
            }

            if (thisItem.runorder != 0){
                thisHTML = thisHTML.replace('~runOrder', '(' + thisItem.runorder + ')');
            } else {
                thisHTML = thisHTML.replace('~runOrder', '');
            }

            thisHTML = thisHTML.replace('~competitorName', thisItem.competitor);
            thisHTML = thisHTML.replace(/~competitorId/g, thisItem.ID);
            thisHTML = thisHTML.replace('~inputa', thisItem.scorea);
            thisHTML = thisHTML.replace('~inputb', thisItem.scoreb);
            thisHTML = thisHTML.replace('~inputc', thisItem.scorec);
            thisHTML = thisHTML.replace(/~row/g, i);


            thisHTML = thisHTML.replace('~total', utils.scoresToTotal(thisItem.scorea, thisItem.scoreb, thisItem.scorec));
            thisHTML = thisHTML.replace('~theme', utils.placeToTheme(thisItem.place));
            $thisUL.append(thisHTML).trigger('create');

        }

        $thisUL.listview('refresh');
        if (status != 2){
            utils.updateScoringLeaders();

            scoresApp.webdb.getCompetitorsForNameSelect(utils.getUrlVars().eventid);
        }
        utils.hideOverlay();
    };


    var PopulatePossibleWinnerList = function(data){

        if (! data.rows.length){
            utils.relocate('./index.html?showMessageA=No competitors were found for event');
            return;
        }

        var $baseTemplate = templates.scoreResultTable();
        var $base = $('#baseItem');
        var $winners = 0;
        var $selOption = 0;
        for (var i = 0; i < data.rows.length; i++){
            thisItem = data.rows.item(i);
            var $thisHTML = $baseTemplate;


            if (thisItem.runorder != 0){
                $thisHTML = $thisHTML.replace('~runOrder', '(' + thisItem.runorder + ')');
            } else {
                $thisHTML = $thisHTML.replace('~runOrder', '');
            }

            $thisHTML = $thisHTML.replace('~competitor', thisItem.competitor);
            $thisHTML = $thisHTML.replace('~score', thisItem.total);
            $thisHTML = $thisHTML.replace(/~id/g, thisItem.ID);
            $base.append($thisHTML);
            $base.trigger('create');

            $winners++;

            $selOption = ($winners > 3) ? 0 : $winners;

            var $selBox = '#contestant_' + thisItem.ID;
            $($selBox).prop('selectedIndex', $selOption );
            $($selBox).selectmenu('refresh');

            if (data.rows.item(i+1).total == 0){
                break;
            }
            if ($winners >= 3 && thisItem.total != data.rows.item(i+1).total){
                break;
            }

        }

        utils.hideOverlay();

    };

    var FinalizeResults = function(eventid){
       utils.relocate('./event.html?source=0&eventid='+ eventid +'', 1);
    };

    var PopulateNameSelect = function(data){
        var populated = false;
        if (! data.rows.length){
            return;
        }
         var $select = $('#competitorNameSelect');

        for (var i = 0; i < data.rows.length; i++){
            thisItem = data.rows.item(i);
            if (thisItem.competitor.indexOf('Competitor') == -1){
                var optTempl = '<option value="' +thisItem.competitor+ '">'+thisItem.competitor+'</option>';
                $select.append(optTempl);
                populated = true;
            }
        }

        if ( populated == false){
            $select.parent().hide();
        } else {
            $select.selectmenu();
            $select.selectmenu('refresh', true);
        }



    };

    var EventDeleteNotify = function(result){
        if (result){
            var x = scoresApp.webdb.deleteEvent($('.deleteEventBtn').attr('eventid'));
            utils.relocate('./index.html?showMessageS=Event Deleted');
        }
    };

    return {

        newEventSaveComplete: function(id){         
            NewEventSaveComplete(id);
        },
        eventUpdateComplete: function(id){
            NewEventSaveComplete(id);
        },
        populatePage: function(page, data){
            PopulatePage(page, data);            
        },
        populateDisciplineList: function(data){
            PopulateDisciplineList(data);
        },
        populateDisciplineListScoring: function(data){
            PopulateDisciplineListScoring(data);
        },
        populateDisciplineEditList: function(data){
            PopulateDisciplineEditList(data);
        },
        populateScoringFields: function(data, status){
            PopulateScoringFields(data, status);
        },
        populatePossibleWinnerList: function(data){
            PopulatePossibleWinnerList(data);
        },
        finalizeResults: function(eventid){
            FinalizeResults(eventid);
        },
        populateNameSelect: function(data){
            PopulateNameSelect(data);
        },
        confirmCallBack: function(result, caller){
            if(caller == 'eventDelete'){
                EventDeleteNotify(result);
            }
        }


        
        
    }

}();
 
// end callbacks


//utility functions 
var utils = function(){
 
    var IsDate = function(val) {
         var d = new Date(val);
         return !isNaN(d.valueOf());
    };

    var pad = function(number, length) {
        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }

        return str;
    };

    var DateFormat= function(srcdate){
         var d = new Date(srcdate);
         var curr_date = d.getDate();
         var curr_month = d.getMonth() + 1; //Months are zero based
         var curr_year = d.getFullYear();
         return curr_month + "/" + curr_date + "/" + curr_year;

    };

    var DateFormatForDB= function(srcdate){
        if (srcdate.indexOf("-")){
            var dateArray = srcdate.split('-');
        } else {
            return DateFormat(srcdate);
        }
        var curr_date = dateArray[2];
        var curr_month = dateArray[1];
        var curr_year = dateArray[0];
        return curr_month + "/" + curr_date + "/" + curr_year;
    };



    var DateFormatForm= function(srcdate){
        var d = new Date(srcdate);
        var curr_date = d.getDate();
        var curr_month = d.getMonth()+ 1; //Months are zero based
        var curr_year = d.getFullYear();

        return curr_year + '-' + pad(curr_month, 2) + "-" + pad(curr_date, 2);
    };
    var GetUrlVars = function() {
         var vars = [], hash;
         var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

         if (window.location.href.indexOf('?') == -1){
             return vars;
         }

         for(var i = 0; i < hashes.length; i++)
         {
             hash = hashes[i].split('=');
             vars.push(hash[0]);
             vars[hash[0]] = hash[1];
         }
         return vars;
    };


    var Logger = function(data, type){
         if (type != 1){
            console.log(data);
         } else {
            console.dir(data);
         }
    };

    var reloc = function(page, direction){

        if (direction == 1) {
            isReverse = false;
        } else {
            isReverse = true;
        }

        $.mobile.changePage(page, {
            transition: "slide",
            reverse: isReverse,
            changeHash: true,
            reloadPage: true
        });

    };

    var StatusToTheme = function(status){

        if (status  == 0 ){
            return 'd';
        } else if (status == 1){
            return 'f';
        } else if (status == 2) {
            return 'b'
        }

        return 'a';


    };

    var EventStatusToTheme = function(status){

        if (status  == 0 ){
            return 'f';
        } else if (status == 1){
            return 'b';
        }else if (status == 2){
            return 'b';
        }

        return 'a';


    };

    var ScoresToTotal = function(a, b, c){
        return  a + b + c;
    };

    var PlaceToTheme = function(place){
        if (place  == 0 || place > 3 ){
            return 'e';
        } else if (place == 1){
            return 'd';
        } else if (place == 2){
            return 'b';
        } else if (place == 3){
            return 'f';
        }

    };

    var PlaceToText = function(place){

        if (place == 1){
            return '1st';
        } else if (place == 2){
            return '2nd';
        } else if (place == 3){
            return '3rd';
        }
        return '';
    };



    var SwitchFocus = function(row, item){
        // disabled till I can get it working
        return;
        if (item == 'c'){
            return;
        } else if (item == 'b'){
            $item = '#score3_' + row;
        } else if (item == 'a'){
            $item = '#score2_' + row;
        }

        $($item).focus();

    };

    var UpdateScoringLeaders = function(){
        var $items = $('.totalField');
        var $itemArray = [];
        $.each($items,  function(e, i){
            $itemArray.push({place: 0, row: $(i).attr('id').split("_")[1], value: parseInt($(i).html())});
        });
        $itemArray.sort(function(a, b){
            var a1= a.value, b1= b.value;
            if(b1== a1) return 0;
            return b1> a1? 1: -1;
        });

        var $places = 1;
        $.each($itemArray,  function(e, i){
            var $thisScore = i.value;
            if (e > 0 && i.value < $itemArray[e-1].value){
                $itemArray[e].place = $places++;
            } else if (e > 0 && i.value == $itemArray[e-1].value) {
                $itemArray[e].place = $places;
            } else { e == 0}{
                $itemArray[e].place = $places;
            }
            if (i.value == 0){
                $itemArray[e].place = 0;
            }

        });

        $.each($itemArray,  function(e, i){
            if ($places < 4){
                i.place = 0;
                // this prevents style from being applied until there are 3 scores entered.
            }

            var $item = '#total_' + i.row;
            var oldTheme = $($item).parents('li').attr('data-theme');
            var newTheme = utils.placeToTheme(i.place);
            var element = $($item).parents('li');

            $($item).parents('li').attr('place', i.place);
            updateElementTheme(element, oldTheme, newTheme);

        });

    };

    var updateElementTheme = function(element, oldTheme, newTheme){
        if( $(element).attr('data-theme') ) {
            $(element).attr('data-theme', newTheme);
        }

        if( $(element).attr('class') ) {
            /* Theme classes end in "-[a-z]$", so match that */
            var classPattern = new RegExp('-' + oldTheme + '$');
            newTheme = '-' + newTheme;

            var classes =  $(element).attr('class').split(' ');

            for( var key in classes ) {
                if( classPattern.test( classes[key] ) ) {
                    classes[key] = classes[key].replace( classPattern, newTheme );
                }
            }
            $(element).attr('class', classes.join(' '));
        }


    };

    var ValidateWinnerChoices = function(){
        var $f = 0, $s = 0, $t = 0, $n = 0;
        var $counter = 0;
        $.each($('select.placeSelect'), function(e, i ){
            $counter++;
            if ($(i).val() == 0){
                $n++;
            } else if ($(i).val() == 1){
                $f++;
            } else if ($(i).val() == 2){
                $s++;
            } else if ($(i).val() == 3){
                $t++;
            }

        });
        if ($f > 1 || $s > 1 || $t > 1){
            return 1;
        } else if ($f = 0 || $s == 0 || $t == 0){
            if ($counter == 2 && ($f != 0 && $s != 0)){
                return 0; // only 2 possible winners
            } else if ($counter == 1 && $f != 0){
                return 0; // only 1 possible winner
            }
            return 2;
        } else {
            return 0;
        }

    };
    var BuildWinnerArray = function(){
        var $placeArray = [];
        $.each($('select.placeSelect'), function(e, i ){
            if ($(i).val() != 0){
                $placeArray.push($(i).attr('id').split("_")[1] + '_' + $(i).val());
            }

        });
        return $placeArray;
    };
    var ChangeHeadertheme = function(status, type){


        var newTheme = (type == 1) ? utils.eventStatusToTheme(status) : utils.statusToTheme(status);
        var removeClasses = 'ui-bar-a ui-bar-b ui-bar-c ui-bar-d ui-bar-e ui-bar-f ui-btn-up-a ui-btn-up-b ui-btn-up-c ui-btn-up-d ui-btn-up-e ui-btn-up-f';
        //change the header's class/attr to relfect the new theme letter
        $.mobile.activePage.children('.ui-header').attr('data-theme', newTheme).removeClass(removeClasses).addClass('ui-bar-' + newTheme);

        //change the header button's classes/attr to reflect the new theme letter
        $.mobile.activePage.children('.ui-header').children('a').attr('data-theme', newTheme).removeClass(removeClasses).addClass('ui-btn-up-' + newTheme);

        return false;


    };


    return {
        checkValues: function(item){
            return CheckValues(item);
        },
        isDate: function(val){
            return IsDate(val);
        },
        dateFormat: function(val){
            return DateFormat(val);
        },
        dateFormatForDB: function(val){
            return DateFormatForDB(val);
        },
        dateFormatForm: function(val){
            return DateFormatForm(val);
        },
        getUrlVars: function(){
            return GetUrlVars();
        },
        logger: function(item, type){
            Logger(item, type);
        },
        relocate: function( page , direction) {
            reloc(page, direction);
        },
        statusToTheme: function( status ) {
            return StatusToTheme(status);
        },
        eventStatusToTheme: function( status ) {
            return EventStatusToTheme(status);
        },
        scoresToTotal: function(a, b, c){
            return ScoresToTotal(a, b, c);

        },
        placeToTheme: function(place){
            return PlaceToTheme(place);
        },
        placeToText: function(place){
          return PlaceToText(place);
        },
        switchFocus: function(row, item){
            SwitchFocus(row, item);
        },
        updateScoringLeaders: function(){
            UpdateScoringLeaders();
        },
        hideOverlay: function(){
            $.mobile.hidePageLoadingMsg();
        },
        showOverlay: function(){
            $.mobile.showPageLoadingMsg("a", "Loading please wait...");
        },
        validateWinnerChoices: function(){
            return ValidateWinnerChoices();
        },
        buildWinnerArray: function(){
            return BuildWinnerArray();
        },
        changeHeadertheme: function(status, type){
            ChangeHeadertheme(status, type);
        }
    }

}();
//end utilities

//form validation functions
var validator = function(){
    var max_chars = 1;

    var CheckValues = function(item, autoMove){
        var thisItem = $(item);
        var thisValue = thisItem.val();
        if (thisValue.length == 0){
            thisValue = 0;
        } else if (thisValue.length > max_chars) {
            thisItem.val(thisValue.substr(0, max_chars));
            return;
        }
        if (Number(thisItem.val()) == 0 && thisItem.val().length){
            thisItem.val('');
            return;
        }

        var thisRow = thisItem.parents('div .ui-grid-c').attr('rowcode');
        var tempCompetitorID = thisItem.parents('li').attr('competitor');
        var vals = Number(checkNumericVal($('#score1_' + thisRow).val())) + Number(checkNumericVal($('#score2_' + thisRow).val())) + Number(checkNumericVal($('#score3_' + thisRow).val()));

        thisItem.parent().parent().find('.totalField').html(vals);

        utils.updateScoringLeaders();

        scoresApp.webdb.updateCompetitorScore(tempCompetitorID, thisItem.attr('field'), thisValue);

        if (autoMove){
            utils.switchFocus(thisRow, thisItem.attr('field'))
        }
    };

    var checkNumericVal = function(item){
        if ($.isNumeric(item)){
            return item;
        } else {
            return 0;
        }
    };
    
    return {
        checkValues: function(item, autoMove){
            CheckValues(item, autoMove);
        },
        checkNumber: function(item){         
            return checkNumericVal(item);
        }
    };

}();


var clickActions = function(){

    var ShowListButtons = function(item){
        $(item).next('div').toggle();
        $(item).parent().next('span').toggle();
        //$(item).parent().next('span').toggleClass('ui-icon-arrow-d').toggleClass('ui-icon-arrow-u');
    };


    var TempStoreCompetitor = function(obj){
        tempCompetitorName = obj.find('span').eq(1).html();
        tempCompetitorID = obj.attr('competitor');

        if (tempCompetitorName.indexOf('Competitor') != 0){
            $('#competitorName').val(tempCompetitorName);
        } else {
            $('#competitorName').val('');
        }
    };

    return {
        showListButtons: function(item){
            ShowListButtons(item);
        },
        deleteEvent: function(item){
            DeleteEvent(item);
        },
        tempStoreCompetitor: function(obj){
            TempStoreCompetitor(obj);

        }
    }


}();
    var templates = function(){

    var disciplineLI = function(){
        template = '<input id="discipline~id" name="discipline" value="~id" type="checkbox"><label for="discipline~id">~name</label>';
        return template;
    };


    var eventlistIP = function(){

        template = '<li data-theme="f">\
                        <a href="./event.html?eventid=~eventid&source=~source" data-transition="slide">\
                            <h3>~eventname</h3>\
                            <p>~eventdate</p>\
                        </a>\
                    </li>';
        return template;
    };

    var EventListComp = function(){

        template = '<li data-theme="b">\
                        <a href="./event.html?eventid=~eventid&source=~source" data-transition="slide">\
                            <h3>~eventname</h3>\
                            <p>~eventdate</p>\
                        </a>\
                    </li>';
        return template;
    };

    var EventDisciplineLi = function(){
        template = '<li data-theme="~theme">\
        <a href="./scoring.html?eventid=~eid&disciplineid=~did" data-transition="slide" data-ajax="false">\
        ~discipline\
        </a>\
    </li>';
        return template;
    };


    var EventDisciplineEditLi = function(){
        template = '<input id="checkbox~id" class="selectedDisciplineEdit" status="~status" name="selectedDiscipline" value="~id" type="checkbox" data-theme="~theme" checked="checked"><label for="checkbox~id">~discipline</label>';
        return template;
    };

    var ScoreEntryLI = function(){
        template = '<li competitor="~competitorId" data-theme="~theme" place="0">\
            <p>\
                <span class="ui-icon-pencil">&nbsp;</span>\
                <a style="text-decoration:  none;" competitor="~competitorId" href="#popupContestantName" onclick="clickActions.tempStoreCompetitor($(this));" data-rel="popup" data-position-to="window"  data-inline="true" data-transition="flip"><span style="color: white; font-weight: bold;">~runOrder </span><span style="color: white; font-weight: bold;">~competitorName</span></a>\
            </p>\
            <div class="ui-grid-c" rowcode="~row">\
            <div class="ui-block-a"><input field="a" type="number" value="~inputa" min="0" max="9" name="name" id="score1_~row" class="restrictNum" data-mini="true"  onfocus="validator.checkValues(this, 0);" oninput="validator.checkValues(this, 1);" /></div>\
            <div class="ui-block-b"><input field="b" type="number" value="~inputb" min="0" max="9" name="name" id="score2_~row" class="restrictNum" data-mini="true" onfocus="validator.checkValues(this, 0);" oninput="validator.checkValues(this, 1);" /></div>\
        <div class="ui-block-c"><input field="c" type="number" value="~inputc" min="0" max="9" name="name" id="score3_~row" class="restrictNum" data-mini="true" onfocus="validator.checkValues(this, 0);" oninput="validator.checkValues(this, 1);" /></div>\
        <div class="ui-block-d" style="text-align:center; margin: 0px;"><p style="margin-top: 8px; margin-bottom: 0px;font-weight: bold; font-style: italic; font-size: 1em;" id="total_~row" class="totalField">~total</p></div>\
        </div>\
        </li>';
        return template;
    };
    var ScoreEntryLIRO = function(){
        template = '<li competitor="~competitorId" data-theme="~theme" place="0">\
        <h4>~placeTxt &nbsp;&nbsp;&nbsp;~runOrder&nbsp;&nbsp;~competitorName</h4><hr>\
        <div class="ui-grid-c" rowcode="~row">\
            <div class="ui-block-a" style="text-align:center; ">~inputa</div>\
            <div class="ui-block-b" style="text-align:center; ">~inputb</div>\
            <div class="ui-block-c" style="text-align:center; ">~inputc</div>\
            <div class="ui-block-d" style="text-align:center; ">~total</div>\
        </div>\
        </li>';
        return template;
    };
    var ScoreResultTable = function(){

      template = '<div class="ui-grid-b" id="baseItem~id">\
          <div class="ui-block-a">\
            <div><p><span>~runOrder</span> ~competitor</p></div>\
            </div>\
        <div class="ui-block-b">\
            <div><p style="text-align: center;">~score</p></div>\
        </div>\
        <div class="ui-block-c">\
            <div data-role="fieldcontain">\
                <select name="place" class="placeSelect" data-mini="true" data-native-menu="false" id="contestant_~id">\
                    <option value="0" selected="selected">-----</option>\
                    <option value="1">First</option>\
                    <option value="2">Second</option>\
                    <option value="3">Third</option>\
                </select>\
            </div>\
            </div>\
        </div>';
        return template;
    };

    return {
        disciplineLi: function() {
            return disciplineLI();
        },
        eventListIp: function() {
            return eventlistIP();
        },
        eventListComp: function(){
            return EventListComp();
        },
        eventDisciplineLi: function(){
            return EventDisciplineLi();
        },
        eventDisciplineEditLi: function(){
            return EventDisciplineEditLi();
        },
        scoreEntryLI: function(){
            return ScoreEntryLI();
        },
        scoreEntryLIRO: function(){
            return ScoreEntryLIRO();
        },
        scoreResultTable: function(){
            return ScoreResultTable();
        }

    }

}();



var notify = function() {


    var Init = function() {
        $.noty.defaults = {
            layout: 'topCenter',
            theme: 'defaultTheme',
            type: 'alert',
            text: '',
            dismissQueue: false, // If you want to use queue feature set this true
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            animation: {
                open: {height: 'toggle'},
                close: {height: 'toggle'},
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            },
            timeout: 2500, // delay for closing event. Set false for sticky notifications
            force: false, // adds notification to the beginning of queue when set to true
            modal: false,
            closeWith: ['click'], // ['click', 'button', 'hover']
            callback: {
                onShow: function() { notifyFired = true;},
                afterShow: function() {notifyFired = false;},
                onClose: function() {notifyFired = false;},
                afterClose: function() {notifyFired = false;}
            },
            buttons: false // an array of buttons
        };


        // attempt to load any 'at load' messages from url

        var urlVarArray = utils.getUrlVars();
        try {
            if (urlVarArray['showMessageS'] != undefined){
                notify.success(decodeURI(urlVarArray['showMessageS']));
            } else if (urlVarArray['showMessageW'] != undefined){
                notify.warn(decodeURI(urlVarArray['showMessageW']));
            } else if (urlVarArray['showMessageA'] != undefined){
                notify.alert(decodeURI(urlVarArray['showMessageA']));
            }
        } catch (e){
        }

    };

    var Alert = function(msg){
        var n = noty({text: msg, type: 'error'});
    };
    var Warn = function(msg){
        var n = noty({text: msg, type: 'warning'});
    };
    var Success = function(msg){
        var n = noty({text: msg, type: 'success'});
    };

    var Confirm = function(msg, caller){

        var n = noty({
            text: msg,
            modal: true,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                    mycallbacks.confirmCallBack(true, caller);
                 }
                },
                {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                    $noty.close();
                    mycallbacks.confirmCallBack(false, caller);
                    }
                }
            ]
        });

    };

    return {
        init: function(){
            Init();
        },
        alert: function(m){
            Alert(m);
        },
        warn: function(m){
            Warn(m);
        },
        success: function(m){
            Success(m);
        },
        confirm: function(m, c){
            Confirm(m, c);
        }

    }

}();
