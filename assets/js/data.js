


var scoresApp = {};
scoresApp.webdb = {};
scoresApp.webdb.db = null;
scoresApp.currentStateNum = 2;

scoresApp.webdb.onError = function(tx, e) {
    alert('An error has occurred processing your request.' + e)
};




scoresApp.webdb.open = function() {
  var dbSize = 5 * 1024 * 1024; // 5MB
  scoresApp.webdb.db = openDatabase("tkdscoring", "1.0", "TKD Tournament Scoring", dbSize);
};


// build default data load

scoresApp.webdb.initDB = function(){

    scoresApp.webdb.db.transaction(
        function (tx) {
    
            // create default tables
            tx.executeSql("CREATE TABLE IF NOT EXISTS events (ID INTEGER PRIMARY KEY ASC, eventname TEXT, eventdate DATETIME, status INTEGER DEFAULT 0)", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS eventdiscipline (ID INTEGER PRIMARY KEY ASC, discipline INTEGER, eventid INTEGER, status INTEGER DEFAULT 0)", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS eventdisciplinecompetitor (ID INTEGER PRIMARY KEY ASC, eventdisciplineid INTEGER, competitor TEXT, scorea INTEGER DEFAULT 0, scoreb INTEGER DEFAULT 0, scorec INTEGER DEFAULT 0, place INTEGER DEFAULT 0)", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS discipline (ID INTEGER PRIMARY KEY ASC, name TEXT)", []);
            tx.executeSql("CREATE TABLE IF NOT EXISTS config (ID INTEGER PRIMARY KEY ASC, state INTEGER, configdate DATETIME)", []);


            // load data into tables
            tx.executeSql('SELECT id FROM config', [], function (tx, results) {
                var len = results.rows.length;
                if (len == 0){
                    var configOn = new Date();
                    tx.executeSql("INSERT INTO config(state, configdate) VALUES (?,?)", [1, configOn]);

                    diciplins = "Traditional Forms,Traditional Weapons,Creative Forms,Creative Weapons,XMA Forms,XMA Weapons";

                    $.each(diciplins.split(",").slice(0), function(a, b){
                        tx.executeSql("INSERT INTO discipline (name) VALUES (?)", [b]);
                    });

                }

            }, scoresApp.webdb.onError);

            scoresApp.webdb.db.transaction(
                function (tx) {
                    tx.executeSql("select * from config ", [], function (tx, results){
                        scoresApp.webdb.doUpdate( results);
                    }, scoresApp.webdb.onError);
                }
            );
        }
     ); 
};



// Data update process
// used to get DB up to date with current version of code if necessary
scoresApp.webdb.doUpdate = function(data){
    var thisItem = data.rows.item(0);

    if (scoresApp.currentStateNum == thisItem.state){
        return;
    }
    scoresApp.webdb.db.transaction(
        function (tx) {
            if (thisItem.state < 2) { // update script for state 2
                var configOn = new Date();
                tx.executeSql("alter table eventdisciplinecompetitor add runorder integer not null default 0", []);
                tx.executeSql("update config set state = ?, configdate = ?", [2, configOn]);
            }
    });

};




// start of new functions

scoresApp.webdb.loadDisciplines = function(page){
    
    scoresApp.webdb.db.transaction(
        function (tx) {    
          tx.executeSql("select * from discipline ", [], function (tx, results){
              mycallbacks.populatePage(page, results);
          }, scoresApp.webdb.onError);
        }
     );    
};


scoresApp.webdb.loadEvents = function(page, status){
    
    scoresApp.webdb.db.transaction(
        function (tx) {    
          tx.executeSql("select * from events where status = ? ", [status], function (tx, results){
              mycallbacks.populatePage(page, results);
          }, scoresApp.webdb.onError);
        }
     );    
}


scoresApp.webdb.loadEventEdit = function(eid, page){
    var eventDetail = '';
    var resultData = '';

    scoresApp.webdb.db.transaction(
        function (tx) {
            tx.executeSql("select * from events where ID = ? ", [eid], function (tx, results){
                eventDetail = results;
            }, scoresApp.webdb.onError);

            tx.executeSql("select * from discipline", [], function (tx, results){
                disciplinesList = results;
            }, scoresApp.webdb.onError);
            tx.executeSql("select * from eventdiscipline ed join discipline d on ed.discipline = d.id where ed.eventid = ? order by d.ID asc", [eid], function (tx, results){

                resultData = {
                    event: eventDetail,
                    disciplines: disciplinesList,
                    selDisciplines: results
                };

                mycallbacks.populatePage(page, resultData);
            }, scoresApp.webdb.onError);
        }
    );
};



scoresApp.webdb.saveNewEvent = function(ename, edate, disciplins){
    scoresApp.webdb.db.transaction(
        function (tx) {
          eventName = ename;
          eventDate = utils.dateFormatForDB(edate);;
           
          tx.executeSql("INSERT INTO events(eventname, eventdate, status) VALUES (?, ?, 0)", [eventName, eventDate]);
          tx.executeSql("select max(id) as newid from events", [], function (tx, results){
              eid = results.rows.item(0).newid;
              $.each(disciplins, function(e, i){
                  tx.executeSql("INSERT INTO eventdiscipline(discipline, eventid, status) VALUES (?, ?, 0)", [i, eid]);
               });
              mycallbacks.newEventSaveComplete(eid);
          }, scoresApp.webdb.onError);
        }
     ); 
    
};

scoresApp.webdb.saveUpdateEvent = function(eid, ename, edate, disciplinesNew, disciplinesCurr){
    scoresApp.webdb.db.transaction(
        function (tx) {
            eventName = ename;
            eventDate = utils.dateFormatForDB(edate);
            eventId = eid;
            tx.executeSql("update events set eventname = ?, eventdate = ? where ID = ?", [eventName, eventDate, eventId]);
            tx.executeSql("delete from eventdiscipline where eventid = ? and id not in ("+ disciplinesCurr.toString()+")", [eventId]);

            $.each(disciplinesNew, function(e, i){
                tx.executeSql("INSERT INTO eventdiscipline (discipline, eventid, status) VALUES (?, ?, 0)", [i, eventId]);
            });

            // clear out any dead competitor data
            tx.executeSql(" delete From eventdisciplinecompetitor where eventdisciplineid not in (select ID from eventdiscipline)");

            // set status of event if all disciplines are complete (could happen if discipline was deleted and remaining are complete
            tx.executeSql("select ID from eventdiscipline where eventid = ? and status != 2", [eid], function (tx, results){
                if (! results.rows.length){
                    tx.executeSql("update events set status = 2 where ID = ?", [eid]);
                }
            }, scoresApp.webdb.onError);

            mycallbacks.eventUpdateComplete(eventId);
        }
    );

};



scoresApp.webdb.loadEventDetail = function(eid, page){
    scoresApp.webdb.db.transaction(
        function (tx) {
            tx.executeSql("select * from events where ID = ? ", [eid], function (tx, results){
                mycallbacks.populatePage(page, results);
            }, scoresApp.webdb.onError);
        }
    );

};

scoresApp.webdb.loadScoringHeaderDetail = function(did, page){
    scoresApp.webdb.db.transaction(
        function (tx) {
            tx.executeSql("select ed.*, e.eventdate, e.eventname, d.name  from eventdiscipline ed join events e on ed.eventid = e.ID join discipline d on ed.discipline = d.id where ed.ID = ? ", [did], function (tx, results){

                mycallbacks.populatePage(page, results);

            }, scoresApp.webdb.onError);
        }
    );

};


scoresApp.webdb.loadDisciplinesForEvent = function(eid){
    scoresApp.webdb.db.transaction(
        function (tx) {
            tx.executeSql("select ed.ID, ed.discipline, d.name, ed.discipline, ed.eventid, ed.status from eventdiscipline ed join discipline d on ed.discipline = d.id where ed.eventid = ? order by discipline asc", [eid], function (tx, results){
                mycallbacks.populateDisciplineList(results);
            }, scoresApp.webdb.onError);
        }
    );

};


scoresApp.webdb.loadDisciplinesForEventScoring = function(eid, did){
    scoresApp.webdb.db.transaction(
        function (tx) {
            tx.executeSql("select ed.ID, ed.discipline, d.name, ed.discipline, ed.eventid, ed.status from eventdiscipline ed join discipline d on ed.discipline = d.id where ed.eventid = ? and ed.ID != ?", [eid, did], function (tx, results){
                mycallbacks.populateDisciplineListScoring(results);
            }, scoresApp.webdb.onError);
        }
    );

};


scoresApp.webdb.loadDisciplinesForEditEvent = function(eid){
    scoresApp.webdb.db.transaction(
        function (tx) {
            tx.executeSql("select * from eventdiscipline ed join discipline d on ed.discipline = d.id where ed.eventid = ? ", [eid], function (tx, results){
                mycallbacks.populateDisciplineEditList(results);
            }, scoresApp.webdb.onError);
        }
    );

};


scoresApp.webdb.deleteEvent = function(eid){
    scoresApp.webdb.db.transaction(
        function (tx) {

            tx.executeSql("select eventid from eventdiscipline where eventid = ?", [eid], function (tx, results){
                if (results.rows.length){
                    tx.executeSql("delete from eventdisciplinecompetitor where eventdisciplineid = ?", [results.rows.item(0).eventid]);
                }
            }, scoresApp.webdb.onError);

            tx.executeSql("select ID from eventdiscipline where eventid = ?", [eid], function (tx, results){
                if (results.rows.length){
                    tx.executeSql("delete from eventdiscipline where eventid = ?",  [eid]);
                }
            }, scoresApp.webdb.onError);

            tx.executeSql("select ID from events where ID = ?", [eid], function (tx, results){
                if (results.rows.length){
                    tx.executeSql("delete from events where ID = ?", [eid]);
                }
            }, scoresApp.webdb.onError);

        }
    );

    return 1;
};




scoresApp.webdb.getCompetitorsForDiscipline = function(edid, status){

    scoresApp.webdb.db.transaction(
        function (tx) {

            var order = '';
            if (status == 2){
                order = 'order by place, ID'
            }
            tx.executeSql("select * from eventdisciplinecompetitor where eventdisciplineid = ? " + order, [edid], function (tx, results){

                if (results.rows.length == 0){
                    // load default competitors
                    scoresApp.webdb.buildDefaultCompetitorsForDiscipline(edid, status);
                } else {
                    mycallbacks.populateScoringFields(results, status);
                }
            }, scoresApp.webdb.onError);
        }
    );
};

scoresApp.webdb.getCompetitorsForNameSelect = function(eid){

    scoresApp.webdb.db.transaction(
        function (tx) {
            tx.executeSql("select distinct edc.competitor from eventdisciplinecompetitor edc join  eventdiscipline ed on edc.eventdisciplineid = ed.ID  where ed.eventid = ? order by edc.competitor", [eid], function (tx, results){
                mycallbacks.populateNameSelect(results);
            }, scoresApp.webdb.onError);
        }
    );
};

scoresApp.webdb.updateCompetitorName = function(edcid, competitor){
    scoresApp.webdb.db.transaction(
        function (tx) {
            tx.executeSql("update eventdisciplinecompetitor set competitor = ? where ID = ? ", [competitor, edcid ]);
        }
    );
};

scoresApp.webdb.updateCompetitorScore = function(edcid, field, score){
    scoresApp.webdb.db.transaction(
        function (tx) {
            if (field == 'a'){
                tx.executeSql("update eventdisciplinecompetitor set scorea = ? where ID = ?", [score,  edcid]);
            } else if (field == 'b'){
                tx.executeSql("update eventdisciplinecompetitor set scoreb = ? where ID = ?", [score,  edcid]);
            } else if (field == 'c'){
                tx.executeSql("update eventdisciplinecompetitor set scorec = ? where ID = ?", [score,  edcid]);
            }

            tx.executeSql('update eventdiscipline set status = 1 where ID = (select eventdisciplineid from eventdisciplinecompetitor where ID = ?)', [edcid]);


        }
    );
};


scoresApp.webdb.buildDefaultCompetitorsForDiscipline = function(edid, status){

    scoresApp.webdb.db.transaction(
        function (tx) {
            for (i = 1; i <= 16; i++){
                var $compName = 'Competitor ' + i;
                tx.executeSql('insert into eventdisciplinecompetitor (eventdisciplineid, competitor, runorder) values (?, ?, ?)', [edid, $compName, i]);
            }

            // circle back to loader
            scoresApp.webdb.getCompetitorsForDiscipline(edid, status);
        }
    );
};



scoresApp.webdb.getPossibleWinners = function(edid){
    scoresApp.webdb.db.transaction(

        function (tx) {
            tx.executeSql("select ID, competitor, runorder, sum(scorea + scoreb + scorec) as total from eventdisciplinecompetitor where eventdisciplineid = ? group by ID order by total desc", [edid], function (tx, results){
                mycallbacks.populatePossibleWinnerList(results);
            }, scoresApp.webdb.onError);
        }
    );

};

scoresApp.webdb.finalizeDisciplineResults = function(scores, did, eid){
    scoresApp.webdb.db.transaction(
        function (tx) {

            // reset all competitors to 0 ; just in case.
            tx.executeSql("update eventdisciplinecompetitor set place = 0 where eventdisciplineid = ?", [did]);

            var item = '';
            $.each(scores, function(e, i){
                item = i.split("_");
                tx.executeSql("update eventdisciplinecompetitor set place = ? where ID = ?", [item[1], item[0]]);

            });

            // set the rest of  the competitors to a high place number for sorting
            tx.executeSql("update eventdisciplinecompetitor set place = 9 where place = 0 and eventdisciplineid = ?", [did]);

            // set status of event discipline
            tx.executeSql("update eventdiscipline set status = 2 where ID = ?", [did]);

            // set status of event if all disciplines are complete
            tx.executeSql("select ID from eventdiscipline where eventid = ? and status != 2", [eid], function (tx, results){
                if (! results.rows.length){
                    tx.executeSql("update events set status = 2 where ID = ?", [eid]);
                }
            }, scoresApp.webdb.onError);

            mycallbacks.finalizeResults(eid);
        }
    );

};




function init() {
    scoresApp.webdb.open();
    scoresApp.webdb.initDB();
  }



init();

